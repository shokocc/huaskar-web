const path = require('path');

const render = (view, options) => (req, res) => {
  res.render(path.join(__dirname, view), {
    req,
    ...options
  });
}

module.exports = render;
