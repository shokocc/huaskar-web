const router = require('express').Router();
const render = require('../middlewares/render.js');
const logger = require('../util/logger.js');

let cache_ip = {};

// clear cache_ip every second
setInterval(() => {
  cache_ip = {};
}, 1000);
router.get('/', (req, res) => {
    if(!cache_ip[req.ip] && !req.ip.includes('127.0.0.1')) {
        cache_ip[req.ip] = true;
        logger.log({
            ip: req.ip,
            query: req.query,
            body: {msg: 'Entraron a la pagina'}
        });
    }
    render('../views/index.pug')(req, res);
});
router.get('/logs', async (req, res) => {
    const logs = await logger.getLogs();

    console.log(logs);
    const ips = logs.map(l => ({
        ip: l.log[0] && l.log[0].ip,
        utm_source: l.log[0] && l.log[0].query && l.log[0].query.utm_source,
        body: l.log[0] && l.log[0].body,
    }));

    render('../views/logs.pug', {ips})(req, res);
});
router.post('/log', (req, res) => {
    logger.log({
        ip: req.ip,
        query: req.query,
        body: req.body
    });
    res.send('logged');
});


module.exports = router;