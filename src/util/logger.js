var fs = require('fs');
var logFile = fs.createWriteStream('globallogs.log', { flags: 'a' });

async function getLogs() {
    const globallogs = await fs.promises.readFile('globallogs.log', 'utf8');
    const logs = globallogs.split('\n').filter(v => !!v).map(log => JSON.parse(log));
    return logs;
}

function log(...args) {
    let obj = {
        date: new Date(),
        log: args,
    };
    logFile.write(JSON.stringify(obj) + '\n');
    console.log(' --- logged --- ');
    console.log(JSON.stringify(obj));
    console.log(' --- /logged --- ');
}

module.exports = { log, getLogs };