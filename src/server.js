// Require Dependencies
const express = require('express');
const path = require('path');
const morgan = require('morgan');
const cookieSession = require('cookie-session');
const cors = require('cors');

// Load .env Enviroment Variables to process.env
require('mandatoryenv').load([
  'PORT',
  'API_URL',
  'SECRET',
], { defineGlobal: true });

// Get port from enviroment
const { PORT } = process.env;

// Instantiate an Express Application
const app = express();

// Accept json requests
app.set('trust proxy', true);
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

app.use(cookieSession({
  name: 'session',
  secret: env.SECRET,
  maxAge: 24 * 60 * 60 * 1000 // 24 hours
}));

// Views configuration
app.set('view engine', 'pug');

app.set('views', path.join(__dirname, 'views'));

app.locals.basedir = __dirname;

app.use(morgan('dev'));

// Assign Routes
app.use(require('./routes/router.js'));

// Public path
app.use(express.static(path.join(__dirname, './static')));

app.use((error, req, res, next) => {
  console.log(error);
  res.send('Try again later');
});

app.get('*', (req, res) => {
  res.redirect('/');
})

// Open Server on selected Port
app.listen(PORT, () => console.info('Server listening on port ', PORT));
